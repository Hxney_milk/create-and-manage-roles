CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvd_rental TO rentaluser;
GRANT SELECT ON TABLE customer TO rentaluser;

CREATE GROUP rental;
GRANT rental TO rentaluser;

GRANT INSERT, UPDATE ON TABLE rental TO rental;

REVOKE INSERT ON TABLE rental FROM rental;

DO $$ 
DECLARE 
    user_info record;
    user_role_name text;
BEGIN
    FOR user_info IN 
        SELECT customer_id, first_name, last_name
        FROM customer c
        WHERE EXISTS (
            SELECT 1 FROM payment p WHERE p.customer_id = c.customer_id
        ) AND EXISTS (
            SELECT 1 FROM rental r WHERE r.customer_id = c.customer_id
        )
    LOOP
        user_role_name := 'client_' || user_info.first_name || '_' || user_info.last_name;
        user_role_name := lower(user_role_name); -- convert to lowercase to account for case sensitivity

    END LOOP;
END $$;


